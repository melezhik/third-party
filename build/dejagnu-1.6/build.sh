##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the dejagnu package.
##
## Author:
##
##     Evan Green 20-May-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBGCC=libgcc_4.9.2
ZLIB=libz_1.2.8
TCL=tcl_8.6.5
EXPECT=expect_5.45

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency "$LIBGCC" || echo "Ignoring libgcc."
    extract_dependency "$ZLIB"
    extract_dependency "$TCL"
    extract_dependency "$EXPECT"
    export EXPECT="$DEPENDROOT/usr/bin/expect"
    export LD_LIBRARY_PATH="$DEPENDROOT/usr/lib"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install-strip prefix="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

