##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the gnupg package.
##
## Author:
##
##     Evan Green 31-Aug-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBGPG_ERROR=libgpg-error_1.24
LIBASSUAN=libassuan_2.4.3
LIBGCRYPT=libgcrypt_1.7.3
LIBKSBA=libksba_1.3.5
LIBNPTH=libnpth_1.2
LIBICONV=libiconv_1.14
LIBREADLINE=libreadline_6.3
LIBNCURSES=libncurses_5.9
GETTEXT=gettext_0.19.8.1
SQLITE=sqlite_3080500
LIBBZ2=bzip2_1.0.6
OPENLDAP=openldap_2.4.44
GNUTLS=gnutls_3.5.3

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBGPG_ERROR"
    extract_dependency "$LIBASSUAN"
    extract_dependency "$LIBGCRYPT"
    extract_dependency "$LIBKSBA"
    extract_dependency "$LIBNPTH"
    extract_dependency "$LIBICONV"
    extract_dependency "$LIBREADLINE"
    extract_dependency "$LIBNCURSES"
    extract_dependency "$GETTEXT"
    extract_dependency "$SQLITE"
    extract_dependency "$LIBBZ2"
    extract_dependency "$OPENLDAP"
    extract_dependency "$GNUTLS"

    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    export CC_FOR_BUILD=gcc
    export SQLITE3_CFLAGS=" "
    export SQLITE3_LIBS="-lsqlite3"
    export LIBGNUTLS_CFLAGS=" "
    export LIBGNUTLS_LIBS="-lgnutls"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --enable-symcryptrun \
                                     --with-libgpg-error-prefix="$DEPENDROOT/usr" \
                                     --with-libgcrypt-prefix="$DEPENDROOT/usr" \
                                     --with-libassuan-prefix="$DEPENDROOT/usr" \
                                     --with-ksba-prefix="$DEPENDROOT/usr" \
                                     --with-npth-prefix="$DEPENDROOT/usr" \
                                     --with-libintl-prefix="$DEPENDROOT/usr" \
                                     --with-libiconv-prefix="$DEPENDROOT/usr" \
                                     --with-zlib="$DEPENDROOT/usr" \
                                     --with-bzip2="$DEPENDROOT/usr" \
                                     --with-readline="$DEPENDROOT/usr" \
                                     --with-ldap="$DEPENDROOT/usr" \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

