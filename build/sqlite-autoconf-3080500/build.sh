##
## Copyright (c) 2014 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the sqlite3 package.
##
## Author:
##
##     Evan Green 8-Jul-2014
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBGCC=libgcc_4.9.2

##
## Export some variables needed to build correctly on Windows.
##

if test "x$BUILD_OS" = "xwin32"; then
    CPPFLAGS="$CPPFLAGS -I$OUTPUT_DIRECTORY/include"
    LDFLAGS="$LDFLAGS -L$OUTPUT_DIRECTORY/lib"
fi

##
## Remove -gstabs+ from CFLAGS as there are too many symbols for stabs.
##

CFLAGS=`echo "$CFLAGS" | sed 's/-gstabs+//'`

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency "$LIBGCC" || echo "Ignoring libgcc."
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --enable-threadsafe=no \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS" \

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install-strip prefix="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

