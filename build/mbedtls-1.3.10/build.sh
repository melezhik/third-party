##
## Copyright (c) 2013 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script builds the mbedtls (formerly PolarSSL) package.
##
## Author:
##
##     Evan Green 16-Apr-2015
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $SOURCE_DIRECTORY
case $BUILD_COMMAND in
  configure)
    echo "No configure necessary."
    ;;

  build)
    export CC="$TARGET-gcc"
    export AR="$TARGET-ar"

    ##
    ## -fomit-frame-pointer is necessary because PolarSSL hand crafts some
    ## assembly that uses R7 in Thumb mode.
    ##

    export CFLAGS="$CFLAGS -fPIC -fomit-frame-pointer"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export SHARED=1
    make $PARALLEL_MAKE no_test
    make $PARALLEL_MAKE install DESTDIR=$OUTPUT_DIRECTORY
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

