##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the expect utility.
##
## Author:
##
##     Evan Green 20-May-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

V=5.45
mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
OLDPWD=`pwd`
cd "$PACKAGE_DIRECTORY/usr/lib"
ln -sfv expect${V}/libexpect${V}.so libexpect${V}.so
cd "$OLDPWD"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: expect
Depends: libgcc, libz, tcl
Priority: optional
Version: $V
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://prdownloads.sourceforge.net/expect/expect5.45.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Expect test framework.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

