##
## Copyright (c) 2013 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##              <gmp> <mpfr> <mpc>
##
## Abstract:
##
##     This script configures and makes the GCC compiler package.
##
## Author:
##
##     Evan Green 24-Oct-2013
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export AR='ar'
    GMP_DIRECTORY="$5"
    if ! test -d "$GMP_DIRECTORY"; then
      echo "$0: Error: GMP directory '$GMP_DIRECTORY' must be set and exist."
      exit 1
    fi

    MPFR_DIRECTORY="$6"
    if ! test -d "$MPFR_DIRECTORY"; then
      echo "$0: Error: MPFR directory '$MPFR_DIRECTORY' must be set and exist."
      exit 1
    fi

    MPC_DIRECTORY="$7"
    if ! test -d "$MPC_DIRECTORY"; then
      echo "$0: Error: MPC directory '$MPC_DIRECTORY' must be set and exist."
      exit 1
    fi

    if test "x$ARCH" = "xarmv6"; then
      ARCH_CONFIG_ARGS="--with-arch=armv6zk --with-fpu=vfp --disable-libatomic"
    fi

    case "$ARCH" in
    armv7) WITH_MODE="--with-mode=thumb" ;;
    esac

    CXXFLAGS="$CFLAGS"
    export CXXFLAGS_FOR_TARGET=" $CXXFLAGS"
    [ "$BUILD_OS" = "macos" ] && \
        CXXFLAGS="$CXXFLAGS -fbracket-depth=512"

    sh ${SOURCE_DIRECTORY}/configure --prefix="$OUTPUT_DIRECTORY" \
                                     --with-sysroot="$OUTPUT_DIRECTORY" \
                                     --with-build-sysroot="$OUTPUT_DIRECTORY" \
                                     --enable-version-specific-runtime-libs \
                                     --with-gmp="$GMP_DIRECTORY" \
                                     --with-mpfr="$MPFR_DIRECTORY" \
                                     --with-mpc="$MPC_DIRECTORY" \
                                     --target=$TARGET \
                                     --enable-languages=c,c++ \
                                     --disable-nls \
                                     --disable-plugin \
                                     --enable-thread=posix \
                                     --disable-win32-registry \
                                     --enable-initfini-array \
                                     --with-gnu-ld \
                                     $WITH_MODE \
                                     $ARCH_CONFIG_ARGS \
                                     CFLAGS="$CFLAGS" \
                                     CXXFLAGS="$CXXFLAGS" \

    ;;

  ##
  ## Configure for building a native Minoca compiler (host and target are the
  ## same).
  ##

  configure)
    export CC="$TARGET-gcc"
    GMP="$5"
    MPFR="$6"
    MPC="$7"
    if test "x$ARCH" = "xarmv6"; then
      ARCH_CONFIG_ARGS="--with-arch=armv6zk --with-fpu=vfp --disable-libatomic"
    fi

    case "$ARCH" in
    armv7) WITH_MODE="--with-mode=thumb" ;;
    esac

    ##
    ## TODO: Remove "--disable-plugin" after upgrading to GCC 5.4 or greater.
    ## This version of GCC fails to build a generator program correctly when
    ## building a compiler for a different OS.
    ##

    [ "$BUILD_OS" = "minoca" ] || DISABLE_PLUGIN="--disable-plugin"
    extract_dependency "$GMP"
    extract_dependency "$MPFR"
    extract_dependency "$MPC"
    sh ${SOURCE_DIRECTORY}/configure --prefix="/usr" \
                                     --with-sysroot="/" \
                                     --with-build-sysroot="$OUTPUT_DIRECTORY/" \
                                     --enable-version-specific-runtime-libs \
                                     --with-gmp="$DEPENDROOT/usr" \
                                     --with-mpfr="$DEPENDROOT/usr" \
                                     --with-mpc="$DEPENDROOT/usr" \
                                     $BUILD_LINE \
                                     --host=$TARGET \
                                     --target=$TARGET \
                                     --enable-languages=c,c++ \
                                     --disable-nls \
                                     --enable-threads=posix \
                                     --disable-bootstrap \
                                     $DISABLE_PLUGIN \
                                     --enable-multilib \
                                     --with-gnu-ld \
                                     $WITH_MODE \
                                     $ARCH_CONFIG_ARGS \
                                     CFLAGS="$CFLAGS" \
                                     CXXFLAGS="$CFLAGS"

    ;;

  build-all-gcc)
    make $PARALLEL_MAKE all-gcc
    ;;

  build-tools)
    export TMPDIR=.
    make $PARALLEL_MAKE
    make install
    ;;

  build)
    export TMPDIR=.
    make $PARALLEL_MAKE
    make install-strip DESTDIR="$OUTPUT_DIRECTORY/"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

