##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages GCC.
##
## Author:
##
##     Evan Green 13-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

GCC_VERSION=4.9.2

##
## Create the libgcc package.
##

rm -rf "$PACKAGE_DIRECTORY"
mkdir -p "$PACKAGE_DIRECTORY/usr/lib"
mkdir -p "$PACKAGE_DIRECTORY/CONTROL"
OLDPWD=`pwd`
GCCLIB="$BUILD_DIRECTORY/usr/lib/gcc/$TARGET/$GCC_VERSION"
cd "$PACKAGE_DIRECTORY/usr/lib"
if [ "$ARCH" = "x86" ] || [ "$ARCH" = "x64" ]; then
    cp -pv "$GCCLIB/libgcc_s.so.1" .
    ln -sf libgcc_s.so.1 libgcc_s.so
fi

cp -pv "$GCCLIB/libstdc++.so.6.0.20" .
ln -sf libstdc++.so.6.0.20 libstdc++.so.6
ln -sf libstdc++.so.6 libstdc++.so
cp -pv "$GCCLIB/libssp.so.0.0.0" .
ln -sf libssp.so.0.0.0 libssp.so.0
ln -sf libssp.so.0 libssp.so

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cd "$OLDPWD"
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libgcc
Priority: optional
Version: $GCC_VERSION
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnu.org/gnu/gcc/gcc-4.9.2/gcc-4.9.2.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: GCC Runtime library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

##
## Create the GCC package.
##

rm -rf "$PACKAGE_DIRECTORY"
mkdir -p "$PACKAGE_DIRECTORY/CONTROL"
cp -Rv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"
rm -rf "$PACKAGE_DIRECTORY/usr/share"

REMOVE_LIBS="libc.so.1
libminocaos.so.1
libcrypt.so.1
libiberty.a"

cd "$PACKAGE_DIRECTORY/usr/lib"
for file in $REMOVE_LIBS; do
    rm -f "$file"
done
cd "$OLDPWD"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: gcc
Depends: binutils, libgcc, libiconv
Priority: optional
Version: $GCC_VERSION
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnu.org/gnu/gcc/gcc-4.9.2/gcc-4.9.2.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Native GNU C compiler.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

