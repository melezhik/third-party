##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and builds the Python setuptools package.
##
## Author:
##
##     Chris Stevens 15-Jun-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

PYTHON2=python2_2.7.11

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  build)
    if test "x$BUILD_OS" != "xminoca"; then
        echo "Error: Python setuptools cannot be cross-compiled."
        exit 3
    fi

    extract_dependency "$PYTHON2"
    export PATH="$DEPENDROOT/usr/bin:$PATH"
    export LD_LIBRARY_PATH="$DEPENDROOT/usr/lib:$LD_LIBRARY_PATH"
    cd ${SOURCE_DIRECTORY}
    python setup.py build --executable="/usr/bin/python" \
                    install --prefix="/usr"              \
                            --exec-prefix="/usr"         \
                            --root ${OUTPUT_DIRECTORY}
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

