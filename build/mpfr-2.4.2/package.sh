##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the MPFR library.
##
## Author:
##
##     Evan Green 13-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

##
## Libtool sucks.
##

rm $PACKAGE_DIRECTORY/usr/lib/*.la

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libmpfr
Depends: libgmp
Priority: optional
Version: 2.4.2
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnu.org/gnu/mpfr/mpfr-2.4.2.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: The MPFR library is a C library for multiple-precision
 floating-point computations with correct rounding.
 MPFR is based on the GMP multiple-precision library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

