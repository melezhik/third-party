################################################################################
#
#   Copyright (c) 2013 Minoca Corp. All Rights Reserved.
#
#   Module Name:
#
#       mpfr-2.4.2
#
#   Abstract:
#
#       This makefile is responsible for building the MPFR package, a
#       prerequisite of GCC.
#
#   Author:
#
#       Evan Green 24-Oct-2013
#
#   Environment:
#
#       Build
#
################################################################################

include ../common.mk

PACKAGE := mpfr-2.4.2
GMP := libgmp_4.3.2

TOOL_GMP_DIRECTORY := $(OBJROOT)/gmp-4.3.2.tool/build.out

PATCHED_SOURCE_DIRECTORY := $(OBJROOT)/$(PACKAGE).src
ORIGINAL_SOURCE_DIRECTORY := $(OBJROOT)/$(PACKAGE).orig
SOURCE_TARBALL := $(ROOT)/third-party/src/$(PACKAGE).tar.gz

TOOLBUILDROOT := $(OBJROOT)/$(PACKAGE).tool
TOOLBINROOT := $(TOOLBUILDROOT)/build.out
BUILDROOT := $(OBJROOT)/$(PACKAGE).build
BINROOT := $(BUILDROOT)/build.out
PKGROOT := $(BUILDROOT)/build.pkg

DIFF_FILE := $(CURDIR)/$(PACKAGE).diff

.PHONY: all clean recreate-diff tools package

all: $(BUILDROOT)/Makefile
	@echo Building $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(BUILDROOT)" "$(BINROOT)" build
	$(MAKE) package

clean:
	rm -rf $(BUILDROOT)
	rm -rf $(TOOLBUILDROOT)
	rm -rf $(PATCHED_SOURCE_DIRECTORY)
	rm -rf $(ORIGINAL_SOURCE_DIRECTORY)

package:
	sh ./package.sh "$(BINROOT)" "$(PKGROOT)"

##
## This target recreates the diff file from the patched source directory.
##

recreate-diff: $(ORIGINAL_SOURCE_DIRECTORY)
	cd $(PATCHED_SOURCE_DIRECTORY) && diff -Nru3 -x .svn ../$(PACKAGE).orig . > $(DIFF_FILE) || test $$? = "1"

##
## Unpack the source to PACKAGE.orig.
##

$(ORIGINAL_SOURCE_DIRECTORY): $(SOURCE_TARBALL)
	rm -rf $(ORIGINAL_SOURCE_DIRECTORY)
	tar -xzf $^ -C $(OBJROOT)
	mv $(OBJROOT)/$(PACKAGE) $@

tools: $(TOOLBUILDROOT)/Makefile
	@echo Building $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(TOOLBUILDROOT)" "$(TOOLBINROOT)" build-tools

$(TOOLBUILDROOT)/Makefile: | $(TOOLBINROOT) $(TOOLBUILDROOT) $(PATCHED_SOURCE_DIRECTORY)
	@echo Configuring $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(TOOLBUILDROOT)" "$(TOOLBINROOT)" configure-tools "$(TOOL_GMP_DIRECTORY)"

$(BUILDROOT)/Makefile: | $(BINROOT) $(BUILDROOT) $(PATCHED_SOURCE_DIRECTORY)
	@echo Configuring $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(BUILDROOT)" "$(BINROOT)" configure "$(GMP)"

##
## Unpack the source to PACKAGE.src, then apply the patch.
##

$(PATCHED_SOURCE_DIRECTORY): $(SOURCE_TARBALL) | $(OBJROOT)
	rm -rf $(PATCHED_SOURCE_DIRECTORY)
	tar -xzf $(SOURCE_TARBALL) -C $(OBJROOT)
	mv $(OBJROOT)/$(PACKAGE) $@
	cd $(PATCHED_SOURCE_DIRECTORY) && $(PATCH) -p0 -i $(DIFF_FILE)

$(TOOLBUILDROOT) $(BUILDROOT) $(BINROOT) $(TOOLBINROOT):
	mkdir -p $@

