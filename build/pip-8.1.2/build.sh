##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and builds the Python pip package.
##
## Author:
##
##     Chris Stevens 16-Jun-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

SETUPTOOLS=python-setuptools_23.0.0

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  build)
    if test "x$BUILD_OS" != "xminoca"; then
        echo "Error: Python pip cannot be cross-compiled."
        exit 3
    fi

    extract_dependency "$SETUPTOOLS"
    export PATH="$DEPENDROOT/usr/bin:$PATH"
    export LD_LIBRARY_PATH="$DEPENDROOT/usr/lib:$LD_LIBRARY_PATH"
    cd ${SOURCE_DIRECTORY}
    python setup.py build --executable="/usr/bin/python" \
                    install --prefix="/usr"              \
                            --exec-prefix="/usr"         \
                            --root ${OUTPUT_DIRECTORY}
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

