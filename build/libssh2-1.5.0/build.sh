##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the libssh2 library.
##
## Author:
##
##     Evan Green 2-Apr-2015
##
## Environment:
##
##     Build
##

. ../build_common.sh

OPENSSL=libopenssl_1.0.2h
LIBGCC=libgcc_4.9.2
ZLIB=libz_1.2.8

if ! extract_dependency "$OPENSSL"; then
    echo "Not building, OpenSSL is missing."
    exit 0
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency "$LIBGCC" || echo "Ignoring libgcc."
    OPENSSL_LINE="--with-openssl --with-libssl-prefix=$DEPENDROOT/usr/"
    extract_dependency "$OPENSSL" || OPENSSL_LINE=
    extract_dependency "$ZLIB"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     $OPENSSL_LINE \
                                     --with-libz \
                                     --with-libz-prefix="$DEPENDROOT/usr/" \
                                     --enable-shared \
                                     --with-pic \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install-strip prefix="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

