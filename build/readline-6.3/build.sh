##
## Copyright (c) 2014 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the readline package.
##
## Author:
##
##     Evan Green 22-Jul-2014
##
## Environment:
##
##     Build
##

. ../build_common.sh

NCURSES=libncurses_5.9

##
## Export some variables needed to cross compile correctly.
##

if test "$BUILD_OS" != "minoca"; then
    export bash_cv_wcwidth_broken=no
    CPPFLAGS="$CPPFLAGS -I$OUTPUT_DIRECTORY/include"
    LDFLAGS="$LDFLAGS -L$OUTPUT_DIRECTORY/lib"
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$NCURSES"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --with-curses \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS" \

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install prefix="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

