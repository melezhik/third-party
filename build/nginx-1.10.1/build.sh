##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the nginx web server package.
##
## Author:
##
##     Evan Green 22-Jun-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

OPENSSL=libopenssl_1.0.2h
ZLIB=libz_1.2.8
PCRE=libpcre_8.39
LIBGCC=libgcc_4.9.2

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    cp -Rp ${SOURCE_DIRECTORY}/* .
    export CC="$TARGET-gcc"
    extract_dependency "$OPENSSL"
    extract_dependency "$ZLIB"
    extract_dependency "$PCRE"
    extract_dependency "$LIBGCC" || echo "Skipping libgcc."
    export CFLAGS="$CFLAGS"
    export CPPFLAGS="-I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    ##
    ## Remove libtool nonsense.
    ##

    rm $DEPENDROOT/usr/lib/*.la || echo whatever rm
    sh ./configure --conf-path=/etc/nginx/nginx.conf \
                   --pid-path=/var/run/nginx.pid \
                   --lock-path=/var/run/nginx.lock \
                   --error-log-path=/var/log/nginx/error.log \
                   --http-log-path=/var/log/nginx/access.log \
                   --user=www-data --group=www-data \
                   --with-http_ssl_module \
                   --without-select_module \
                   --prefix="/usr" \
                   --with-cc-opt="$CFLAGS $CPPFLAGS" \
                   --with-ld-opt="$LDFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

