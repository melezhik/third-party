##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the postgresql package.
##
## Author:
##
##     Evan Green 16-Aug-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBGCC=libgcc_4.9.2
NCURSES=libncurses_5.9
READLINE=libreadline_6.3
LIBZ=libz_1.2.8

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$NCURSES"
    extract_dependency "$READLINE"
    extract_dependency "$LIBZ"
    extract_dependency "$LIBGCC" || echo "Ignoring libgcc."

    ##
    ## Postgres doesn't cross compile the time zone data. Just assume it's
    ## going to be there (even though it really isn't).
    ##

    [ "$BUILD_OS" != "minoca" ] && \
        SYSTEM_TZDATA=--with-system-tzdata=/usr/share/postgresql/timezone

    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --enable-thread-safety \
                                     $SYSTEM_TZDATA \
                                     --disable-rpath \
                                     --prefix="/usr" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    make
    make install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

