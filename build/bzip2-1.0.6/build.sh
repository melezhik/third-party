##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the bzip2 package.
##
## Author:
##
##     Evan Green 7-Mar-2015
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $SOURCE_DIRECTORY/
case $BUILD_COMMAND in
  build)
    make CC="$TARGET-gcc" AR="$TARGET-ar" RANLIB="$TARGET-ranlib" \
         CFLAGS="$CFLAGS -Wall -Winline -D_FILE_OFFSET_BITS=64" \
         PREFIX="$OUTPUT_DIRECTORY" \
         install

    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

