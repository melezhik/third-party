##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the boost package.
##
## Author:
##
##     Chris Stevens 19-Jul-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

PYTHON2=python2_2.7.11
BZIP2=bzip2_1.0.6
LIBZ=libz_1.2.8

export CXXFLAGS="$CFLAGS -std=c++11"
export PATH="$DEPENDROOT/usr/bin:$PATH"
export LIBRARY_PATH="$DEPENDROOT/usr/lib:$LIBRARY_PATH"
export LD_LIBRARY_PATH="$DEPENDROOT/usr/lib:$LD_LIBRARY_PATH"

cd $SOURCE_DIRECTORY
case $BUILD_COMMAND in
  configure)

    ##
    ## Boost depends on Python, so it cannot be cross-compiled.
    ##

    if test "x$BUILD_OS" != "xminoca"; then
        echo "Error: Boost depends on Python, which cannot cross-compile."
        exit 3
    fi

    extract_dependency "$PYTHON2"
    extract_dependency "$LIBZ"
    extract_dependency "$BZIP2"
    sh ${SOURCE_DIRECTORY}/bootstrap.sh --prefix="/usr"
    ;;

  build)

    ##
    ## As bzip2 and zlib are not in normal locations, tell boost where it can
    ## find the headers and the libraries.
    ##

    DEPEND_INCLUDE="$DEPENDROOT/usr/include"
    DEPEND_LIB="$DEPENDROOT/usr/lib"
    export BZIP2_INCLUDE="$DEPEND_INCLUDE"
    export BZIP2_LIBPATH="$DEPEND_LIB"
    export ZLIB_INCLUDE="$DEPEND_INCLUDE"
    export ZLIB_LIBPATH="$DEPEND_LIB"

    ##
    ## b2 does take a -jN parameter, but this causes systems with 1GB or less
    ## to page, which significantly slows down the build process.
    ##

    ./b2 install link=shared \
                 threading=multi \
                 --build-dir="$BUILD_DIRECTORY" \
                 --prefix="$OUTPUT_DIRECTORY/usr" \
                 --without-context \
                 --without-coroutine \
                 --without-coroutine2 \
                 --without-test

    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

