##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the expat library.
##
## Author:
##
##     Evan Green 18-Mar-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBGCC=libgcc_4.9.2

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBGCC" || echo "Ignoring libgcc."
    CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --with-pic \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS" \

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

