##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the pkg-config package.
##
## Author:
##
##     Evan Green 15-Aug-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBICONV=libiconv_1.14

if [ "$BUILD_OS" != "minoca" ]; then
    export glib_cv_stack_grows=no
    export glib_cv_uscore=no
    export ac_cv_func_posix_getpwuid_r=yes
    export ac_cv_func_posix_getgrgid_r=yes
fi

##
## TODO: Remove --with-internal-glib if glib is added.
##

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBICONV"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --with-internal-glib \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

