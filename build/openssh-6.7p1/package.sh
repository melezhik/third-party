##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the OpenSSH utility.
##
## Author:
##
##     Evan Green 16-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/sbin" "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/etc/ssh"
cp -pv "$BUILD_DIRECTORY/etc/ssh/ssh_config" "$PACKAGE_DIRECTORY/etc/ssh"
cp -pv "$BUILD_DIRECTORY/etc/ssh/sshd_config" "$PACKAGE_DIRECTORY/etc/ssh"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/etc/init.d"
cat > "$PACKAGE_DIRECTORY/etc/init.d/sshd" <<_EOS
#! /bin/sh
set -e

# /etc/init.d/ssh: start and stop the sshd "secure shell" daemon

test -x /usr/sbin/sshd || exit 0
( /usr/sbin/sshd -\? 2>&1 | grep -q OpenSSH ) 2>/dev/null || exit 0

[ -f /etc/init.d/init-functions ] && . /etc/init.d/init-functions

umask 022
if test -f /etc/default/ssh; then
    . /etc/default/ssh
fi

if [ -n \$2 ]; then
    SSHD_OPTS="\$SSHD_OPTS \$2"
fi

check_for_no_start() {
    # forget it if we're trying to start, and /etc/ssh/sshd_not_to_be_run exists
    if [ -e /etc/ssh/sshd_not_to_be_run ]; then
        log_action_msg \
                "Secure Shell server not in use (/etc/ssh/sshd_not_to_be_run)"

        exit 0
    fi
}

check_privsep_dir() {
    # Create the PrivSep empty dir if necessary
    if [ ! -d /var/run/sshd ]; then
        mkdir -p /var/run/sshd
        chmod 0755 /var/run/sshd
    fi
}

check_config() {
    /usr/sbin/sshd -t || exit 1
}

check_keys() {
    # create keys if necessary
    if [ ! -f /etc/ssh/ssh_host_rsa_key ]; then
        log_daemon_msg "  generating ssh RSA key..."
        ssh-keygen -q -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
    fi
    if [ ! -f /etc/ssh/ssh_host_dsa_key ]; then
        log_daemon_msg "  generating ssh DSA key..."
        ssh-keygen -q -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa
    fi
}

export PATH="\${PATH:+\$PATH:}/usr/sbin:/sbin"

case "\$1" in
    start)
        check_for_no_start
        log_daemon_msg "Starting OpenSSH server" || true
        check_keys
        check_privsep_dir
        if start-stop-daemon -S -p /var/run/sshd.pid -qo -x /usr/sbin/sshd \
            -- \$SSHD_OPTS ; then

            log_end_msg 0 || true

        else
            log_end_msg 1 || true

        fi
        ;;

    stop)
        log_daemon_msg "Stopping OpenSSH server" || true
        if start-stop-daemon -K -p /var/run/sshd.pid -qo -n sshd; then
            log_end_msg 0 || true

        else
            log_end_msg 1 || true
        fi

        ;;

    reload|force-reload)
        check_for_no_start
        check_keys
        check_config
        check_privsep_dir
        log_daemon_msg "Reloading OpenSSH server's configuration" || true
        if start-stop-daemon -K -p /var/run/sshd.pid -qo -s 1 -n sshd; then
            log_end_msg 0 || true

        else
            log_end_msg 1 || true
        fi
        ;;

    restart)
        check_keys
        check_config
        log_daemon_msg "Restarting OpenSSH server"
        start-stop-daemon -K -p /var/run/sshd.pid -qo -n sshd
        check_for_no_start
        check_privsep_dir
        sleep 2
        if start-stop-daemon -S -p /var/run/sshd.pid -qo -x /usr/sbin/sshd \
            -- \$SSHD_OPTS; then

            log_end_msg 0 || true

        else
            log_end_msg 1 || true
        fi
        ;;

    *)
        log_action_msg \
            "Usage: /etc/init.d/ssh {start|stop|reload|force-reload|restart}"

        exit 1
        ;;

esac

exit 0
_EOS
chmod +x "$PACKAGE_DIRECTORY/etc/init.d/sshd"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: openssh
Depends: libopenssl
Priority: optional
Version: 6.7p1
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp5.usa.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-6.7p1.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Secure rlogin/rsh/rcp/telnet replacement (OpenSSH) Ssh
 (Secure Shell) is a program for logging into a remote machine and for
 executing commands on a remote machine. It provides secure encrypted
 communications between two untrusted hosts over an insecure network.
 X11 connections and arbitrary TCP/IP ports can also be forwarded over the
 secure channel. It is intended as a replacement for rlogin, rsh and rcp, and
 can be used to provide applications with a secure communication channel.
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/conffiles" <<_EOS
/etc/ssh/sshd_config
/etc/ssh/ssh_config
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/prerm" <<"_EOS"
#! /bin/sh
if test "x$D" != "x"; then
    exit 1
else
    /etc/init.d/sshd stop
    userdel sshd
    groupdel sshd
    update-rc.d -f sshd remove
    rm -f /etc/init.d/sshd
fi
_EOS

chmod +x "$PACKAGE_DIRECTORY/CONTROL/prerm"

cat > "$PACKAGE_DIRECTORY/CONTROL/postinst" <<"_EOS"
#! /bin/sh

set -x
ROOTLINE=
CHROOTCMD=
if test "$PKG_ROOT" != "/"; then
    ROOTLINE="-R$PKG_ROOT"
    CHROOTCMD="chroot $PKG_ROOT -- "
fi

if test "x$D" != "x"; then
    exit 1

else
    useradd $ROOTLINE --system --home=/var/run/sshd --no-create-home \
        --shell=/bin/false sshd

    $CHROOTCMD /usr/sbin/update-rc.d sshd defaults
fi
_EOS

chmod +x "$PACKAGE_DIRECTORY/CONTROL/postinst"
create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

