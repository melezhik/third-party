##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the autogen package.
##
## Author:
##
##     Evan Green 27-Jul-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBICONV=libiconv_1.14
LIBTOOL=libtool_2.2.8
LIBUNISTRING=libunistring_0.9.6
LIBFFI=libffi_3.2.1
GC=libgc_7.4.4
READLINE=libreadline_6.3
GUILE=guile_2.0.12

export LD_LIBRARY_PATH="$DEPENDROOT/usr/lib"
export GUILE_SYSTEM_PATH="$DEPENDROOT/usr/share/guile/2.0"
export GUILE_SYSTEM_COMPILED_PATH="$DEPENDROOT/usr/lib/guile/2.0/ccache"
cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBICONV"
    extract_dependency "$LIBTOOL"
    extract_dependency "$LIBUNISTRING"
    extract_dependency "$LIBFFI"
    extract_dependency "$READLINE"
    extract_dependency "$GC"
    extract_dependency "$GUILE"
    export CC="$TARGET-gcc"
    export GUILE_CFLAGS=" -I$DEPENDROOT/usr/include/guile/2.0"
    export GUILE_LIBS="-lguile-2.0"
    export GUILE_EFFECTIVE_VERSION="2.0"
    export gdir="$DEPENDROOT/usr/include/guile/2.0"
    CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
-Wl,-rpath-link=$DEPENDROOT/usr/lib"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

