##
## Copyright (c) 2014 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the ncurses package.
##
## Author:
##
##     Evan Green 10-Jul-2014
##
## Environment:
##
##     Build
##

. ../build_common.sh

##
## Export some variables needed to build correctly on Windows.
##

if test "x$BUILD_OS" = "xwin32"; then

    ##
    ## Make on Windows seems to crash when autoconf probes for makeflags.
    ##

    export cf_cv_makeflags='-${MAKEFLAGS}'
    EXE='.exe'
fi

if test "x$BUILD_OS" = "xminoca"; then
    EXTRA_MAKEFLAGS='shell=SH'
fi

##
## Disallow mixed case filenames so that the database can be built on Windows,
## which isn't case sensitive.
##

export cf_cv_mixedcase=no

##
## Add the output bin directory to the path, as the tic program is used during
## the build process.
##

export PATH="$PATH${PATH_SEPARATOR-:}$OUTPUT_DIRECTORY/bin"

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export AR='ar'
    sh ${SOURCE_DIRECTORY}/configure --with-install-prefix="$OUTPUT_DIRECTORY" \
                                     --prefix=/ \
                                     --enable-overwrite \
                                     --without-ada \
                                     --enable-term-driver \
                                     --enable-sp-funcs \
                                     --enable-big-core \
                                     --without-manpages \
                                     CFLAGS="$CFLAGS" \

    ;;

  configure)
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --with-install-prefix="$OUTPUT_DIRECTORY" \
                                     --prefix=/usr \
                                     --enable-overwrite \
                                     --without-ada \
                                     --enable-big-core \
                                     --without-manpages \
                                     --with-shared \
                                     CFLAGS="$CFLAGS"

    ;;

  build-tools)
    make $PARALLEL_MAKE
    for app in tic$EXE infocmp$EXE; do
      cp -v ${BUILD_DIRECTORY}/progs/$app ${OUTPUT_DIRECTORY}/bin/$app
    done
    ;;

  build)
    make $PARALLEL_MAKE $EXTRA_MAKEFLAGS
    make $EXTRA_MAKEFLAGS install.progs
    make $EXTRA_MAKEFLAGS install.includes
    make $EXTRA_MAKEFLAGS install.libs
    TERMINFO_DIR=${OUTPUT_DIRECTORY}/usr/share/terminfo
    mkdir -p "$TERMINFO_DIR"
    tic -x -s -e'ansi, ansi-m, xterm-256color, linux, putty, putty-vt100, vt100, vt102, vt220, xterm' -o "$TERMINFO_DIR" ${SOURCE_DIRECTORY}/misc/terminfo.src
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

