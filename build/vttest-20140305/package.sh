##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the vttest utility.
##
## Author:
##
##     Evan Green 16-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/bin"
cp -Rpv "$BUILD_DIRECTORY/bin/vttest" "$PACKAGE_DIRECTORY/usr/bin"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: vttest
Priority: optional
Version: 20140305
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://invisible-island.net/datafiles/release/vttest.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: vttest is a utility that tests compliance of VT terminal emulators.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

