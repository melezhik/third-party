##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the Tcl scripting language package.
##
## Author:
##
##     Evan Green 23-May-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBGCC=libgcc_4.9.2
ZLIB=libz_1.2.8

export UNAME=Minoca
export CC="$TARGET-gcc"
if [ "$BUILD_OS" != "Minoca" ]; then
    export tcl_cv_sys_version=Minoca
    export tcl_cv_strtod_buggy=no
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBGCC" || echo "Ignoring libgcc."
    extract_dependency "$ZLIB"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    sh ${SOURCE_DIRECTORY}/unix/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --enable-threads \
                                     --enable-shared \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    set -x
    sed -e "s|$DEPENDROOT||g" \
        -e "s|${SOURCE_DIRECTORY}|/usr/include|g" \
        -e "s|${BUILD_DIRECTORY}|/usr/lib|g" \
        "${BUILD_DIRECTORY}/tclConfig.sh" > "${BUILD_DIRECTORY}/tclConfig.sh2"

    mv "${BUILD_DIRECTORY}/tclConfig.sh2" "${BUILD_DIRECTORY}/tclConfig.sh"
    if [ -r "${BUILD_DIRECTORY}/pkgs/tdbc1.0.4/tdbcConfig.sh" ]; then
        sed -e "s|${BUILD_DIRECTORY}/pkgs/tdbc1.0.4|/usr/lib/tdbc1.0.4|g" \
            -e "s|${SOURCE_DIRECTORY}/pkgs/tdbc1.0.4/generic|/usr/include|g" \
            -e "s|${SOURCE_DIRECTORY}/pkgs/tdbc1.0.4/library|/usr/lib/tcl8.6|g"\
            -e "s|${SOURCE_DIRECTORY}/pkgs/tdbc1.0.4|/usr/include|g" \
            "${BUILD_DIRECTORY}/pkgs/tdbc1.0.4/tdbcConfig.sh" > ./tdbcConfig.sh2

        mv ./tdbcConfig.sh2 "${BUILD_DIRECTORY}/pkgs/tdbc1.0.4/tdbcConfig.sh"
    fi

    if [ -r "${BUILD_DIRECTORY}/pkgs/itcl4.0.4/itclConfig.sh" ]; then
        sed -e "s|${BUILD_DIRECTORY}/pkgs/itcl4.0.4|/usr/lib/itcl4.0.4|g" \
            -e "s|${SOURCE_DIRECTORY}/pkgs/itcl4.0.4/generic|/usr/include|g" \
            -e "s|${SOURCE_DIRECTORY}/pkgs/itcl4.0.4|/usr/include|g" \
            "${BUILD_DIRECTORY}/pkgs/itcl4.0.4/itclConfig.sh" > ./itclConfig.sh2

        mv ./itclConfig.sh2 "${BUILD_DIRECTORY}/pkgs/itcl4.0.4/itclConfig.sh"
    fi

    make install DESTDIR="$OUTPUT_DIRECTORY"
    make install-private-headers DESTDIR="$OUTPUT_DIRECTORY"
    cd ${OUTPUT_DIRECTORY}/usr/bin && ln -vsf tclsh8.6 tclsh
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

