##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the subversion package.
##
## Author:
##
##     Evan Green 29-Mar-2015
##
## Environment:
##
##     Build
##

. ../build_common.sh

SQLITE=sqlite_3080500
OPENSSL=libopenssl_1.0.2h
APR=libapr_1.5.1
APRUTIL=libaprutil_1.5.4
ZLIB=libz_1.2.8
LIBGCC=libgcc_4.9.2
EXPAT=expat_2.1.0

APRLIBDIR="$DEPENDROOT/usr/lib/apr-1"
cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency "$SQLITE"
    extract_dependency "$OPENSSL" || echo "Skipping OpenSSL."
    [ -f /usr/include/apr-1/apr_version.h ] || {
        extract_dependency "$APR"
        WITH_APR=--with-apr="$DEPENDROOT/usr"
    }

    [ -f /usr/include/apr-1/apu_version.h ] || {
        extract_dependency "$APRUTIL"
        WITH_APR_UTIL=--with-apr-util="$DEPENDROOT/usr"
    }

    extract_dependency "$ZLIB"
    extract_dependency "$EXPAT"
    extract_dependency "$LIBGCC" || echo "Ignoring libgcc."
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    ##
    ## Fix up apr-1-config.
    ##

    sed -e "s|^prefix=.*|prefix=\"$DEPENDROOT/usr\"|" \
        -e "s|^installbuilddir=.*|installbuilddir=\"$APRLIBDIR\"|" \
        "$DEPENDROOT/usr/bin/apr-1-config" > \
        $DEPENDROOT/usr/bin/apr-1-config.tmp

    mv "$DEPENDROOT/usr/bin/apr-1-config.tmp" "$DEPENDROOT/usr/bin/apr-1-config"
    chmod +x "$DEPENDROOT/usr/bin/apr-1-config"

    ##
    ## Remove libtool nonsense.
    ##

    rm $DEPENDROOT/usr/lib/*.la || echo whatever rm
    EXPAT_LINE="$DEPENDROOT/usr/include/apr-1:$DEPENDROOT/usr/lib:expat"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-nls \
                                     $WITH_APR \
                                     $WITH_APR_UTIL \
                                     --with-sqlite="$DEPENDROOT/usr/" \
                                     --with-zlib="$DEPENDROOT/usr/" \
                                     --with-expat="$EXPAT_LINE" \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

