##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the cURL package.
##
## Author:
##
##     Evan Green 31-Mar-2015
##
## Environment:
##
##     Build
##

. ../build_common.sh

OPENSSL=libopenssl_1.0.2h
LIBGCC=libgcc_4.9.2
ZLIB=libz_1.2.8
LIBSSH2=libssh2_1.5.0
MBEDTLS=libmbedtls_1.3.10

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    SSL_LINE="--with-ssl=$DEPENDROOT/usr/"
    LIBSSH2_LINE="--with-libssh2=$DEPENDROOT/usr"
    extract_dependency "$OPENSSL" || SSL_LINE=
    if test -z "$SSL_LINE"; then
        extract_dependency "$MBEDTLS" && \
                SSL_LINE="--with-polarssl=$DEPENDROOT/usr"
    fi

    extract_dependency "$LIBGCC" || echo "Ignoring libgcc."
    extract_dependency "$ZLIB"
    extract_dependency "$LIBSSH2" || LIBSSH2_LINE=
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-nls \
                                     --with-pic=yes \
                                     --enable-shared \
                                     --with-ca-bundle=/etc/ssl/certs/ca-bundle.crt \
                                     --with-ca-path=/etc/ssl/certs/ \
                                     $SSL_LINE \
                                     $LIBSSH2_LINE \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install-strip prefix="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

