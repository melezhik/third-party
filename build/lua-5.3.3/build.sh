##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the Lua package.
##
## Author:
##
##     Evan Green 20-Jan-2014
##
## Environment:
##
##     Build
##

. ../build_common.sh

NCURSES=libncurses_5.9
READLINE=libreadline_6.3

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$READLINE"
    extract_dependency "$NCURSES"
    cp -Rp ${SOURCE_DIRECTORY}/* .
    ;;

  build)
    export CC="$TARGET-gcc"
    export AR="$TARGET-ar"
    export RANLIB="$TARGET-ranlib"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    cd src
    make $PARALLEL_MAKE PLAT=minoca "MYCFLAGS=$CFLAGS" "MYLDFLAGS=$LDFLAGS" \
         LIBS="-lreadline" CC="$CC" AR="$AR rcu" RANLIB="$RANLIB"

    cd "$BUILD_DIRECTORY"
    make install PLAT=minoca INSTALL_TOP="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

