##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the MySQL package.
##
## Author:
##
##     Chris Stevens 29-Jun-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

NCURSES=libncurses_5.9
CMAKE=cmake_3.5.2
BOOST=boost_1_59_0

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)

    ##
    ## Cross-compiling MySQL is easier said than done. Don't allow it for now.
    ##

    if test "x$BUILD_OS" != "xminoca"; then
        echo "Error: MySQL does not cross-compile easily."
        exit 3
    fi

    ##
    ## MySQL expects either C++11 or deprecated pre-C99 macros.
    ##

    export CXXFLAGS="$CFLAGS -std=c++11"

    ##
    ## MySQL depends on ncurses, boost and uses cmake to configure. Use the
    ## versions that were built during the build process.
    ##

    extract_dependency "$NCURSES"
    extract_dependency "$CMAKE"
    extract_dependency "$BOOST"
    export PATH="$DEPENDROOT/usr/bin:$PATH"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    cmake -D CMAKE_FIND_ROOT_PATH="$DEPENDROOT" \
          -D CMAKE_C_FLAGS="$CFLAGS" \
          -D CMAKE_CXX_FLAGS="$CXXFLAGS" \
          -D CMAKE_EXE_LINKER_FLAGS="$LDFLAGS" \
          -D CMAKE_MODULE_LINKER_FLAGS="$LDFLAGS" \
          -D WITH_BOOST="$DEPENDROOT/usr/include" \
          -D CMAKE_INSTALL_PREFIX="/usr" ${SOURCE_DIRECTORY}

    ;;

  build)
    make COLOR=0
    make install/strip DESTDIR="$OUTPUT_DIRECTORY" COLOR=0
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

