##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the Python core.
##
## Author:
##
##     Evan Green 16-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/include" "$PACKAGE_DIRECTORY/usr"
cp -Rp "$BUILD_DIRECTORY/lib" "$PACKAGE_DIRECTORY/usr"

##
## Remove all the test junk and some other bloat.
##

rm -rf "$PACKAGE_DIRECTORY/usr/lib/python2.7/bsddb/test"
rm -rf "$PACKAGE_DIRECTORY/usr/lib/python2.7/distutils/tests"
rm -rf "$PACKAGE_DIRECTORY/usr/lib/python2.7/email/test"
rm -rf "$PACKAGE_DIRECTORY/usr/lib/python2.7/idlelib"
rm -rf "$PACKAGE_DIRECTORY/usr/lib/python2.7/json/tests"
rm -rf "$PACKAGE_DIRECTORY/usr/lib/python2.7/lib2to3/tests"
rm -rf "$PACKAGE_DIRECTORY/usr/lib/python2.7/lib-tk"
rm -rf "$PACKAGE_DIRECTORY/usr/lib/python2.7/sqlite3/test"
rm -rf "$PACKAGE_DIRECTORY/usr/lib/python2.7/ctypes/test"
rm -rf "$PACKAGE_DIRECTORY/usr/lib/python2.7/test"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: python2
Depends: libopenssl, libreadline, libncurses, sqlite, expat
Priority: optional
Version: 2.7.11
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://www.python.org/ftp/python/2.7.11/Python-2.7.11.tgz
Installed-Size: $INSTALLED_SIZE
Description: Python is an interpreted scripting language.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

