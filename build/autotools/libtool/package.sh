##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the libtool utilities.
##
## Author:
##
##     Evan Green 19-May-2016
##
## Environment:
##
##     Build
##

. ../../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/share"
rm "$BUILD_DIRECTORY"/usr/lib/*.la
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/share/aclocal" "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/usr/share/libtool" "$PACKAGE_DIRECTORY/usr/share"

##
## Get the version number (ie 1.5.24).
##

PKG_VERSION=`echo $BUILD_DIRECTORY | sed -n 's|.*libtool-\(.*\)\.build.*|\1|p'`
if [ -z "$PKG_VERSION" ]; then
    echo "Error: PKG_VERSION came up empty."
    exit 1
fi

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libtool
Priority: optional
Version: $PKG_VERSION
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://mirrors.kernel.org/gnu/libtool/libtool-$PKG_VERSION.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: GNU libtool scripts.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

