##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the autoconf packages (all versions).
##
## Author:
##
##     Evan Green 18-May-2016
##
## Environment:
##
##     Build with POSIX tools.
##

. ../../build_common.sh

##
## Get the version number (ie 2.64).
##

PKG_VERSION=`echo $SOURCE_DIRECTORY | sed -n 's|.*autoconf-\(.*\)\.src.*|\1|p'`
if [ -z "$PKG_VERSION" ]; then
    echo "Error: PKG_VERSION came up empty."
    exit 1
fi

if test "x$BUILD_OS" = "xwin32"; then

    ##
    ## Unfortunately Perl backtick commands send each word as a separate
    ## argument, so simply setting it to "sh -c" would get expanded to
    ## "sh -c 'word0' '...'", when it really should be "sh -c 'word0 ...'".
    ## This "script" repackages the command line to be in a single argument.
    ##

    export PERL5SHELL='sh -c "sh -c \\"\$*\\"" dummy'
    export TMPDIR="$BUILD_DIRECTORY"
    export ac_cv_path_PERL='perl'
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export M4="$TOOLBINROOT/bin/m4"
    sh ${SOURCE_DIRECTORY}/configure --prefix="$OUTPUT_DIRECTORY" \
                                     --program-suffix=-$PKG_VERSION \

    ;;

  configure)
    export M4="m4"
    sh ${SOURCE_DIRECTORY}/configure --prefix="/usr" \
                                     --program-suffix=-$PKG_VERSION \

    ;;

  build | build-tools)
    touch ${SOURCE_DIRECTORY}/man/*.1
    make
    make install prefix="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

