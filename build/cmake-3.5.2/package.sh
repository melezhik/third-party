##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the cmake utility.
##
## Author:
##
##     Chris Stevens 22-Jun-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/share" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: cmake
Recommends: make
Priority: optional
Version: 3.5.2
Architecture: $PACKAGE_ARCH
Maintainer: Chris Stevens <chris@minocacorp.com>
Section: main
Source: https://cmake.org/files/v3.5/cmake-3.5.2.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: CMake is a compiler-independent build process management tool.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

