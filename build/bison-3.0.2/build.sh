##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the bison package.
##
## Author:
##
##     Evan Green 9-Jan-2015
##
## Environment:
##
##     Build
##

. ../build_common.sh

##
## Export some variables needed to build correctly on Windows.
##

INSTALL_TARGET=install-strip
EXTRA_MAKE_ARGS=
if test "$BUILD_OS" != "minoca"; then
    export gl_cv_func_working_strerror=yes
    export gl_cv_func_strerror_0_works=yes
    export gl_cv_func_unsetenv_works=yes
    INSTALL_TARGET=install-exec-am
    EXTRA_MAKE_ARGS="MANS= doc_bison_TEXINFOS= "
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-nls \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE $EXTRA_MAKE_ARGS
    make $PARALLEL_MAKE $INSTALL_TARGET $EXTRA_MAKE_ARGS \
        prefix="$OUTPUT_DIRECTORY"

    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

