##
## Copyright (c) 2014 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the gzip package.
##
## Author:
##
##     Evan Green 15-May-2014
##
## Environment:
##
##     Build
##

. ../build_common.sh

##
## Export some variables needed to cross compile correctly.
##

if test "$BUILD_OS" != "minoca"; then
    export ac_cv_path_shell='sh'
    export gl_cv_func_getopt_gnu=yes
    export gl_cv_func_fflush_stdin=yes
    export gl_cv_func_memchr_works=yes
    export gl_cv_func_working_strerror=yes
    export gl_cv_func_strerror_0_works=yes
    export gl_cv_func_gettimeofday_clobber=no
    export ac_cv_func_malloc_0_nonnull=yes
    export gl_cv_func_setenv_works=yes
    export gl_cv_func_unsetenv_works=yes
    export gl_cv_func_fdopendir_works=yes
    export ac_cv_func_realloc_0_nonnull=yes
    export ac_cv_func_calloc_0_nonnull=yes
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install-strip prefix="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

