################################################################################
#
#   Copyright (c) 2013 Minoca Corp. All Rights Reserved.
#
#   Module Name:
#
#       Third Party
#
#   Abstract:
#
#       This makefile is responsible for building third party packages.
#
#   Author:
#
#       Evan Green 23-Oct-2013
#
#   Environment:
#
#       Build
#
################################################################################

include build/common.mk

##
## Define the minimal set of tools needed to build the OS repository.
##

OS_TOOLS := awk                  \
            binutils-2.23        \
            gmp-4.3.2            \
            mpfr-2.4.2           \
            mpc-0.8.1            \
            gcc-4.9.2            \
            byacc-20141128       \
            acpica-unix-20140424 \

ifneq ($(BUILD_OS),win32)

OS_TOOLS += flex-2.5.39         \

endif

##
## Define the set of tools needed to build the entire third-party repository.
##

ALL_TOOLS := $(OS_TOOLS)          \
             ncurses-5.9          \
             dmake-DMAKE_4_12_2_2 \
             openssl-1.0.2h       \
             m4-1.4.17            \
             autotools/autoconf   \
             autotools/automake   \
             autotools/libtool    \
             cmake-3.5.2          \
             ninja-1.7.1          \

##
## Define the set of packages that are compiled as tools for the build machine
## but may not be compiled for Minoca. The reason for this is usually that the
## tool cannot be cross compiled.
##

TOOLS_ONLY := perl-5.20.1        \

##
## Define all the packages, omitting any packages listed in the tools section
## above.
##

APPS := zlib-1.2.8               \
        libiconv-1.14            \
        make-3.82                \
        tar-1.27.1               \
        gzip-1.6                 \
        patch-2.7                \
        sqlite-autoconf-3080500  \
        readline-6.3             \
        nano-2.2.6               \
        expat-2.1.0              \
        vttest-20140305          \
        xz-5.2.1                 \
        bzip2-1.0.6              \
        pcre-8.39                \
        wget-1.15                \
        opkg-0.2.4               \
        shadow-4.2.1             \
        apr-1.5.1                \
        apr-util-1.5.4           \
        subversion-1.8.11        \
        bash-4.3.30              \
        dash-0.5.8               \
        mbedtls-1.3.10           \
        libssh2-1.5.0            \
        curl-7.41.0              \
        git-2.3.5                \
        vim-7.4                  \
        openssh-6.7p1            \
        ca-certificates          \
        bison-3.0.2              \
        tcl8.6.5                 \
        pcre2-10.21              \
        libxml2-2.9.4            \
        httpd-2.4.20             \
        lua-5.3.3                \
        less-481                 \
        libevent-2.0.22-stable   \
        tmux-2.2                 \
        libunistring-0.9.6       \
        libffi-3.2.1             \
        libatomic_ops-7.4.4      \
        gc-7.4.4                 \
        dropbear-2016.74         \
        rsync-3.1.2              \
        pkg-config-0.29.1        \
        postgresql-9.5.4         \
        gettext-0.19.8.1         \
        glib-2.49.6              \
        libcroco-0.6.11          \
        gdbm-1.12                \
        zsh-5.2                  \
        libgpg-error-1.24        \
        libassuan-2.4.3          \
        pinentry-0.9.7           \
        libgcrypt-1.7.3          \
        libksba-1.3.5            \
        npth-1.2                 \
        nettle-3.2               \
        libtasn1-4.9             \
        p11-kit-0.23.2           \
        libidn-1.33              \
        unbound-1.5.9            \
        gnutls-3.5.3             \
        openldap-2.4.44          \
        gnupg-2.1.15             \
        gpgme-1.6.0              \
        pth-2.0.7                \

##
## Some packages can only be compiled natively (they cannot be cross compiled).
##

ifeq ($(BUILD_OS),minoca)

APPS += perl-5.20.1             \
        Python-2.7.11           \
        expect5.45              \
        dejagnu-1.6             \
        setuptools-23.0.0       \
        pip-8.1.2               \
        nginx-1.10.1            \
        emacs-24.5              \
        screen-4.4.0            \
        guile-2.0.12            \
        boost_1_59_0            \
        mysql-5.7.13            \
        autogen-5.18.10         \
        node-v4.5.0             \
        ruby-2.3.1              \
        php-7.0.9               \

endif

ifeq ($(BUILD_OS),win32)

APPS += flex-2.5.39             \

endif


PACKAGES := $(ALL_TOOLS) $(APPS)

PACKAGES_TOOLS := %-tools
PACKAGES_CLEAN := %-clean
BUILD_STAMP := $(OBJROOT)/%.build/build.stamp
TOOL_STAMP := $(OBJROOT)/%.tool/build.stamp

##
## This function definition, when called with 2 arguments, evaluates to the
## following (paraphrased):
## <1>.stamp: <2>.stamp <3>.stamp ...
## where <2>, <3>, <4>, etc, consist of argument 2 split up on spaces.
##

dep = $(1:%=$(BUILD_STAMP)): $(foreach pkg,$(2),$(pkg:%=$(BUILD_STAMP)))
tool_dep = $(1:%=$(TOOL_STAMP)): $(foreach pkg,$(2),$(pkg:%=$(TOOL_STAMP)))

all: $(PACKAGES)
all-tools: all-tools-quiet
all-tools-quiet: $(ALL_TOOLS:%=$(PACKAGES_TOOLS)) $(TOOLS_ONLY:%=$(PACKAGES_TOOLS))
tools: $(OS_TOOLS:%=$(PACKAGES_TOOLS))
clean: $(PACKAGES:%=$(PACKAGES_CLEAN))

all all-tools tools clean:
	@echo Completed building $@

.PHONY: all all-tools all-tools-quiet tool $(PACKAGES)
.PHONY: $(ALL_TOOLS:%=$(PACKAGES_TOOLS)) $(TOOLS_ONLY:%=$(PACKAGES_TOOLS))
.PHONY: clean $(PACKAGES:%=$(PACKAGES_CLEAN)) package-tools

##
## For packages, just recurse into that directory. Each packages depends on its
## corresponding stamp file, and depends on the tools being built.
## These static pattern rules in the form <1>: <2>: <3> say "for each target
## in <1>, apply pattern <2> to get prerequisites list <3> for that target."
##

$(PACKAGES): %: $(BUILD_STAMP) all-tools-quiet

##
## The packages-tools work similarly, except they depend on the tool stamp
## file. To build ALL_TOOLS and TOOLS_ONLY (with -tools appended to
## differentiate from PACKAGES), get the stamp file for that tool.
##

$(ALL_TOOLS:%=$(PACKAGES_TOOLS)) $(TOOLS_ONLY:%=$(PACKAGES_TOOLS)): $(PACKAGES_TOOLS): $(TOOL_STAMP)

##
## To build the stamps, just touch them. Each stamp is dependent on its package.
##

$(PACKAGES:%=$(BUILD_STAMP)):
	@echo Making $@
	@$(MAKE) -C build/$(@:$(BUILD_STAMP)=%)
	@echo Done making $@
	@mkdir -p `dirname $@`
	@date > $@

$(ALL_TOOLS:%=$(TOOL_STAMP)) $(TOOLS_ONLY:%=$(TOOL_STAMP)):
	@echo Making $@
	@$(MAKE) -C build/$(@:$(TOOL_STAMP)=%) tools
	@echo Done making $@
	@mkdir -p `dirname $@`
	@date > $@

##
## To clean a package, run make clean in it.
##

$(PACKAGES:%=$(PACKAGES_CLEAN)):
	@$(MAKE) -C build/$(@:$(PACKAGES_CLEAN)=%) clean

##
## Define dependencies.
##

$(call dep,binutils-2.23,awk)
$(call tool_dep,binutils-2.23,awk)
$(call dep,mpfr-2.4.2,gmp-4.3.2)
$(call tool_dep,mpfr-2.4.2,gmp-4.3.2)
$(call dep,mpc-0.8.1,mpfr-2.4.2)
$(call tool_dep,mpc-0.8.1,mpfr-2.4.2)
$(call dep,gcc-4.9.2,binutils-2.23 gmp-4.3.2 mpfr-2.4.2 mpc-0.8.1 awk)
$(call tool_dep,gcc-4.9.2,binutils-2.23 gmp-4.3.2 mpfr-2.4.2 mpc-0.8.1 awk)
$(call tool_dep,openssl-1.0.2h,perl-5.20.1)
$(call dep,acpica-unix-20140424,byacc-20141128)
$(call tool_dep,acpica-unix-20140424,byacc-20141128)

ifeq ($(BUILD_OS),win32)

$(call tool_dep,perl-5.20.1,dmake-DMAKE_4_12_2_2)

else

$(call tool_dep,acpica-unix-20140424,flex-2.5.39)

endif

$(call dep,Python-2.7.11,sqlite-autoconf-3080500 ncurses-5.9 \
    zlib-1.2.8 openssl-1.0.2h bzip2-1.0.6)

$(call dep,readline-6.3,ncurses-5.9)
$(call dep,nano-2.2.6,readline-6.3)
$(call dep,openssh-6.7p1,openssl-1.0.2h)
$(call dep,apr-util-1.5.4,apr-1.5.1 sqlite-autoconf-3080500,openssl-1.0.2h)
$(call dep,subversion-1.8.11,apr-util-1.5.4)
$(call dep,curl-7.41.0,libssh2-1.5.0)
$(call dep,git-2.3.5,curl-7.41.0 libiconv-1.14 zlib-1.2.8 \
    subversion-1.8.11)

$(call dep,libssh2-1.5.0,openssl-1.0.2h)
$(call dep,curl-7.41.0,libssh2-1.5.0)
$(call dep,autotools/automake,autotools/autoconf)
$(call dep,autotools/libtool,autotools/autoconf)
$(call dep,tcl8.6.5,zlib-1.2.8)
$(call dep,libxml2-2.9.4,zlib-1.2.8 libiconv-1.14 readline-6.3)
$(call dep,httpd-2.4.20,apr-util-1.5.4 pcre-8.39 openssl-1.0.2h libxml2-2.9.4)
$(call dep,lua-5.3.3,readline-6.3)
$(call dep,less-481,ncurses-5.9)
$(call dep,emacs-24.5,ncurses-5.9)
$(call dep,libevent-2.0.22-stable,openssl-1.0.2h)
$(call dep,tmux-2.2,libevent-2.0.22-stable ncurses-5.9)
$(call dep,libunistring-0.9.6,libiconv-1.14)
$(call dep,pkg-config-0.29.1,libiconv-1.14)
$(call dep,gc-7.4.4,libatomic_ops-7.4.4)
$(call dep,screen-4.4.0,ncurses-5.9)
$(call dep,dropbear-2016.74,zlib-1.2.8)
$(call dep,rsync-3.1.2,zlib-1.2.8)
$(call dep,guile-2.0.12,libgc-7.4.4 libffi-3.2.1 libunistring-0.9.6 \
    libiconv-1.14 readline-6.3)

$(call dep,autogen-5.18.10,guile-2.0.12)
$(call dep,node-v4.5.0,openssl-1.0.2h zlib-1.2.8)
$(call dep,postgresql-9.5.4,readline-6.3)
$(call dep,php-7.0.9,libxml2-2.9.4 readline-6.3 gmp-4.3.2 bzip2-1.0.6)
$(call dep,gettext-0.19.8.1,libiconv-1.14 libxml2-2.9.4 \
    libunistring-0.9.6 ncurses-5.9)

$(call dep,glib-2.49.6,libiconv-1.14 pcre-8.39 libffi-3.2.1 gettext-0.19.8.1)
$(call dep,libcroco-0.6.11,glib-2.49.6 libxml2-2.9.4)
$(call dep,gdbm-1.12,libiconv-1.14 gettext-0.19.8.1)
$(call dep,zsh-5.2,ncurses-5.9 libiconv-1.14 pcre-8.39 gdbm-1.12)
$(call dep,libgpg-error-1.24,libiconv-1.14 gettext-0.19.8.1)
$(call dep,libassuan-2.4.3,libgpg-error-1.24)
$(call dep,pinentry-0.9.7,libassuan-2.4.3 libgpg-error-1.24 \
    libiconv-1.14 ncurses-5.9)

$(call dep,libgcrypt-1.7.3,libgpg-error-1.24)
$(call dep,libksba-1.3.5,libgpg-error-1.24)
$(call dep,nettle-3.2,gmp-4.3.2)
$(call dep,p11-kit-0.23.2,libffi-3.2.1 libtasn1-4.9 gettext-0.19.8.1)
$(call dep,libidn-1.33,libiconv-1.14 gettext-0.19.8.1)
$(call dep,unbound-1.5.9,libiconv-1.14 gettext-0.19.8.1 openssl-1.0.2h \
    libevent-2.0.22-stable expat-2.1.0)

$(call dep,gnutls-3.5.3,gmp-4.3.2 nettle-3.2 p11-kit-0.23.2 \
    libiconv-1.14 gettext-0.19.8.1 zlib-1.2.8 libtasn1-4.9 libidn-1.33 \
    unbound-1.5.9)

$(call dep,openldap-2.4.44,openssl-1.0.2h autotools/libtool)
$(call dep,gnupg-2.1.15,libgpg-error-1.24 libassuan-2.4.3 \
    libgcrypt-1.7.3 libksba-1.3.5 npth-1.2 ncurses-5.9 readline-6.3 \
    gettext-0.19.8.1 sqlite-autoconf-3080500 bzip2-1.0.6 openldap-2.4.44)

$(call dep,gpgme-1.6.0,libgpg-error-1.24 libassuan-2.4.3)
$(call dep,expect5.45,tcl8.6.5)
$(call dep,dejagnu-1.6,expect5.45)
$(call dep,setuptools-23.0.0,Python-2.7.11)
$(call dep,pip-8.1.2,setuptools-23.0.0)
$(call dep,nginx-1.10.1,openssl-1.0.2h pcre-8.39 zlib-1.2.8)
$(call dep,boost_1_59_0,Python-2.7.11 bzip2-1.0.6 zlib-1.2.8)
$(call dep,mysql-5.7.13,ncurses-5.9 cmake-3.5.2 boost_1_59_0)

package-tools: tools
	@sh build/package_tools.sh
	@echo Completed packaging tools

